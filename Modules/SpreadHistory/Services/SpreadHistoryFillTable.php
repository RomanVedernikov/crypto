<?php

namespace Modules\SpreadHistory\Services;

use Laravel\Telescope\Telescope;
use Modules\Command\Entities\Command as CommandEntity;
use Modules\Exchange\Services\Spread\Calculate as SpreadCalculate;
use Modules\Logger\Services\Logger;
use Modules\SpreadHistory\Entities\SpreadHistory;
use Modules\Exchange\Services\WebSockets\Config as WebSocketsConfig;

class SpreadHistoryFillTable
{
    const TIMER = 10; // sec

    public $arDataSpread;
    public $arDataSpreadLastData;
    public $obCommandEntity;

    public function __construct()
    {
    }

    public function run()
    {
        Telescope::stopRecording();

        $this->saveCommand();

        while (true) {

            try {

                $this->arDataSpread = SpreadCalculate::getResult();
                $this->saveData();

            } catch (Exception $e) {
                //dd("Error: " . $e->getMessage());
                //Logger::write("Error: " . $e->getMessage());
            }

            sleep(static::TIMER);
        }
    }

    public function saveData()
    {
        if (empty($this->arDataSpread)) {
            return null;
        }
        foreach ($this->arDataSpread as $symbol => $arDataSpread) {

            // Если еще нету, то смотрим адекватный ли спред и профит
            if (empty($this->arDataSpreadLastData[$symbol])) {
                if (
                    !array_key_exists('spread_truncate', $arDataSpread) ||
                    !array_key_exists('profit_truncate', $arDataSpread)
                ) {
                    continue;
                }
                // Спред
                if ($arDataSpread['spread_truncate'] < 0 || $arDataSpread['spread_truncate'] > 1000) {
                    continue;
                }
                // Профит
                if (isset($arDataSpread['profit_truncate']) && $arDataSpread['profit_truncate'] < 1) {
                    continue;
                }
            }

            $arDataSpread['spread'] = $arDataSpread['spread_truncate'] ?? 0;
            $arDataSpread['profit'] = $arDataSpread['profit_truncate'] ?? 0;
            $arDataSpread['price_buy'] = $arDataSpread['arBestDataBuyResult']['price'];
            $arDataSpread['price_sell'] = $arDataSpread['arBestDataSellResult']['price'];
            $arDataSpread['symbol'] = $symbol;

            $arDataLast = array_key_exists($symbol, $this->arDataSpreadLastData ?? []) ?
                $this->arDataSpreadLastData[$symbol] : [];

            // Если данные изменились
            if ($this->compareData($arDataSpread, $arDataLast)) {

                $this->addRow($arDataSpread);
            }

            $this->arDataSpreadLastData[$symbol] = $arDataSpread;
        }
    }

    public function compareData($arData, $arDataLast)
    {
        // Если еще нету, то пишем
        if (empty($arDataLast)) {
            return true;
        }
        // Если спред изменился на более чем 0.1 тогда пишем
        if (abs($arData['spread'] - $arDataLast['spread']) >= 0.1) {
            return true;
        }

        return false;
    }

    public function addRow($arDataSpread)
    {
        //dd($arDataSpread);
        $obSpreadHistory = new SpreadHistory();
        $obSpreadHistory
            ->fill($arDataSpread)
            ->save();
    }

    private function saveCommand()
    {
        $this->obCommandEntity = CommandEntity::create([
            'pid' => getmypid(),
            'command' => 'crypto:spred-history',
            'signature' => 'crypto:spred-history',
        ]);
    }
}
