<?php

namespace Modules\SpreadHistory\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Modules\SpreadHistory\Services\SpreadHistoryFillTable;

class SpreadHistoryComand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crypto:spred-history';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Заполняет таблицу spread-history';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $obFillTable = new SpreadHistoryFillTable();
        $obFillTable->run();
    }
}
