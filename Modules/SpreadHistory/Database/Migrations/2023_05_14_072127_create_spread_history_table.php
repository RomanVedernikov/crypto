<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpreadHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spread_history', function (Blueprint $table) {
            $table->id();
            $table->string('symbol');
            $table->float('profit', 10, 2)->nullable();
            $table->float('spread',10, 2);
            $table->float('price_buy',30, 8)->nullable();
            $table->float('price_sell',30, 8)->nullable();
            $table->float('volume_buy',30, 8)->nullable();
            $table->float('volume_sell',30, 8)->nullable();
            $table->foreignId('exchange_buy')->constrained('exchange');
            $table->foreignId('exchange_sell')->constrained('exchange');
            $table->float('commission_buy',30, 8)->nullable();
            $table->float('commission_sell',30, 8)->nullable();
            $table->float('commission_transfer',30, 8)->nullable();
            $table->float('commission_conversion',30, 8)->nullable();
            $table->float('success_rate')->nullable(); // вероятность успеха
            $table->boolean('hedging')->nullable(); // хеджирование
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spread_history');
    }
}
