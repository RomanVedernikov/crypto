<?php

namespace Modules\SpreadHistory\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class SpreadHistory extends Model
{
    use HasFactory;

    protected $table = 'spread_history';
    protected $fillable = [
        'symbol',
        'profit',
        'spread',
        'price_buy',
        'price_sell',
        'volume_buy',
        'volume_sell',
        'exchange_buy',
        'exchange_sell',
        'commission_buy',
        'commission_sell',
        'commission_transfer',
        'commission_conversion',
        'success_rate',
        'hedging',
    ];

    protected static function newFactory()
    {
        return \Modules\SpreadHistory\Database\factories\SpreadHistoryFactory::new();
    }
}
