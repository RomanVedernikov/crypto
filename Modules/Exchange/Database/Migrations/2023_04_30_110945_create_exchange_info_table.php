<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExchangeInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exchange_info', function (Blueprint $table) {
            $table->id();
            $table->foreignId('exchange_id')->constrained('exchange');
            $table->string('symbol');
            $table->string('status')->nullable();
            $table->string('ws_symbol')->nullable();
            $table->string('base_asset')->nullable();
            $table->string('quote_asset')->nullable();
            $table->integer('base_asset_precision')->nullable();
            $table->integer('quote_asset_precision')->nullable();
            $table->string('order_types')->nullable();
            $table->string('permissions')->nullable();
            $table->boolean('is_spot_trading')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['exchange_id', 'symbol']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exchange_info');
    }
}
