<?php

namespace Modules\Exchange\Services\Command;

use Laravel\Telescope\Telescope;
use Modules\Command\Entities\Command as CommandEntity;
use Modules\Logger\Services\Logger;
use Symfony\Component\Console\Helper\ProgressBar;
use Illuminate\Support\Facades\Storage;

/**
 * Наблюдаем за таблицей command
 * Перезапускаем процесс если отвалился pid
 *
 *
 * Class CheckAndRun
 * @package Modules\Exchange\Services\Command
 */
class CheckAndRun
{
    const PROGRESS_BAR_ENABLE = false;

    const FILE_NAME_PID_COMMAND_CHECK_AND_RUN = 'pid_command_check_and_run.log';

    private $arObCommands = null;
    private $arOptions;

    public function __construct(array $arOptions)
    {
        $this->arOptions = $arOptions;
    }

    public function run()
    {
        Telescope::stopRecording();

        $this->savePid();

        // Выведем progressBar в консоли
        if ($this->isProgressBarEnable()) {
            $progressBar = new ProgressBar($this->arOptions['output']);
            $progressBar->setFormat('debug');
            $progressBar->start();
        }

        while (true) {

            try {

                $this->arObCommands = null;

                /** @var CommandEntity $obCommand */
                foreach ($this->getCommands() as $obCommand) {

                    $bWasRestart = $obCommand->restartCommand();

                    if ($bWasRestart) {
                        Logger::write("Перезапустил команду: " . $obCommand->command);
                        Logger::write("Память скрипта: " . round(memory_get_usage() / (1024 * 1024), 1) . " Mb");
                        if ($this->isProgressBarEnable()) {
                            $progressBar->advance();
                        }
                    }
                }
            } catch (\ErrorException $e) {
                dd("Error: " . $e->getMessage());
                //Logger::write("Error: " . $e->getMessage());
            }

            sleep(2);
        }

    }

    public function getCommands()
    {
        if (!is_null($this->arObCommands)) {
            return $this->arObCommands;
        }
        $arObCommands = CommandEntity::all();
        // Проверим на уникальность!
        $arUniqCommands = [];
        foreach ($arObCommands as $key => $obCommand) {
            // Если такая команда уже есть, то завершим эту
            if (in_array($obCommand->command, $arUniqCommands)) {
                Logger::write("Нашел дубль команды: " . $obCommand->command);
                if ($obCommand->stopCommand()) {
                    // Удаляем команду из Массива команд
                    unset($arObCommands[$key]);
                } else {
                    // Ошибка завершения
                    Logger::write("Не удалось завершить команду: " . $obCommand->command);
                }
            } else {
                $arUniqCommands[] = $obCommand->command;
            }
        }
        $this->arObCommands = $arObCommands ?? [];
        return $this->arObCommands;
    }

    private function isProgressBarEnable()
    {
        return static::PROGRESS_BAR_ENABLE === true;
    }

    public static function savePid()
    {
        Storage::put(static::FILE_NAME_PID_COMMAND_CHECK_AND_RUN, getmypid());
    }

    public static function getCommandPid()
    {
        if (Storage::exists(static::FILE_NAME_PID_COMMAND_CHECK_AND_RUN)) {
            return Storage::get(static::FILE_NAME_PID_COMMAND_CHECK_AND_RUN);
        }
        return false;
    }

}
