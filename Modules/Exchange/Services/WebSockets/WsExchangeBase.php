<?php

namespace Modules\Exchange\Services\WebSockets;

use Illuminate\Support\Str;
use Modules\Command\Entities\Command as CommandEntity;
use Modules\Exchange\Services\Tools\Redis as RedisTools;


class WsExchangeBase
{
    const CASH_KEY_PREFIX_SPOT_BOOK = 'SpotBook';
    const URL_PAGE = ''; // путь запроса без основного домена
    /**
     * Если установить время, то через каждые PING_TIME sec будет отправлен ping
     */
    const PING_TIME = 10; // sec
    /**
     * Нужно сгруппировать подписки на:
     * 1. Кол-во подписок в одном скрипте
     * 2. Кол-во подписок в одном запросе
     */
    // 1. Кол-во подписок в одном скрипте
    const SUBSCRIPTIONS_ONE_SCRIPT = null; // по умолчанию 1 скрипт
    // 2. Кол-во подписок в одном запросе
    const SUBSCRIPTIONS_ONE_REQUEST = null; // по умолчанию все подписки в одном запросе

    /** @var CommandEntity */
    public $obCommandEntity;
    /** @var Config */
    public $config;
    public $exchange_code;
    public $arSubscriptions = null;
    public $arGroupSubscriptions = null;
    public $iCurrentIndexSub = null;
    public $iScriptIndex = null;
    public $flPingTimer = null;


    public function __construct()
    {
        $this->exchange_code = static::EXCHANGE_CODE;
        $this->config = $this->getConfig();
    }

    public function getCashDataFromDataMessage(array $arDataMessage)
    {
        if (empty($arDataMessage)) {
            return false;
        }
        return [
            'sHash' => static::CASH_KEY_PREFIX_SPOT_BOOK . ":" . $this->getExchangeCode(),
            'sKey' => Str::upper($arDataMessage['sSymbol']),
            'arData' => array_merge($arDataMessage['arData'],
                [
                    'exchange' => $this->getExchangeCode(),
                    'symbol' => Str::upper($arDataMessage['sSymbol']),
                    'time' => time()
                ]
            )
        ];
    }

    public function saveCashSpotBook(array $arDataMessage)
    {
        if (empty($arDataMessage)) {
            return false;
        }
        $arDataCash = $this->getCashDataFromDataMessage($arDataMessage);

        return RedisTools::set($arDataCash['sHash'], $arDataCash['sKey'], $arDataCash['arData']);
    }

    public function saveCashSpotBookMany(array $arDataMessages)
    {
        if (empty($arDataMessages)) {
            return false;
        }
        $arDataCash = [];
        foreach ($arDataMessages as $arDataMessage) {
            $arDataCash[] = $this->getCashDataFromDataMessage($arDataMessage);
        }

        return RedisTools::setMany($arDataCash);
    }

    public function setScriptIndex($iScriptIndex = 0)
    {
        $this->iScriptIndex = $iScriptIndex;
    }

    public function getNextSubscription()
    {
        $arSubscriptions = $this->getNextSubscriptions();
        return $this->getSubscriptionsText($arSubscriptions);
    }

    public function getNextSubscriptions()
    {
        if (is_null($this->iCurrentIndexSub)) {
            $this->iCurrentIndexSub = 0;
        } else {
            $this->incrementCurrentIndexSub();
        }
        return $this->getCurrentGroupSubscriptions()[$this->iCurrentIndexSub] ?? [];
    }

    public function incrementCurrentIndexSub()
    {
        $this->iCurrentIndexSub += 1;
        return $this->iCurrentIndexSub;
    }

    public function getCurrentGroupSubscriptions()
    {
        return $this->getGroupSubscriptions()[$this->iScriptIndex] ?? [];
    }

    public function getSubscriptionsText(array $arSubscriptions)
    {
        if (empty($arSubscriptions)) {
            return null;
        }
        $arRequestSubscription = $this->getRequestSubscription($arSubscriptions);

        return $arRequestSubscription ? json_encode($arRequestSubscription) : null;
    }

    public function getGroupSubscriptions()
    {
        if (!is_null($this->arGroupSubscriptions)) {
            return $this->arGroupSubscriptions;
        }
        $this->arGroupSubscriptions = [];
        $iScriptIndex = $iRequestIndex = 0;
        $iCountScriptSubscriptions = $iCountRequestSubscriptions = 1;
        foreach ($this->getSubscriptions() as $iIndex => $arSubscription) {

            $this->arGroupSubscriptions[$iScriptIndex][$iRequestIndex][] = $arSubscription;

            if (
                static::SUBSCRIPTIONS_ONE_REQUEST > 0 &&
                static::SUBSCRIPTIONS_ONE_REQUEST == $iCountRequestSubscriptions
            ) {
                $iRequestIndex++;
                $iCountRequestSubscriptions = 0;
            }
            if (
                static::SUBSCRIPTIONS_ONE_SCRIPT > 0 &&
                static::SUBSCRIPTIONS_ONE_SCRIPT == $iCountScriptSubscriptions
            ) {
                $iScriptIndex++;
                $iRequestIndex = 0;
                $iCountScriptSubscriptions = 0;
            }
            $iCountRequestSubscriptions++;
            $iCountScriptSubscriptions++;
        }

        return $this->arGroupSubscriptions;
    }

    public function getSubscriptions()
    {
        if (!is_null($this->arSubscriptions)) {
            return $this->arSubscriptions;
        }
        $this->arSubscriptions = [];
        $obExchangeInfoSymbols = $this->getExchangeInfoSymbols();
        foreach ($obExchangeInfoSymbols as $obExchangeInfoSymbol) {
            $this->arSubscriptions[] = $this->getSubscription($obExchangeInfoSymbol);
        }

        return $this->arSubscriptions;
    }

    public function getConfig()
    {
        return new Config($this->exchange_code);
    }

    public function getExchangeCode()
    {
        return $this->exchange_code;
    }

    public function getExchangeId()
    {
        return $this->config->arExchangeInfo['exchange_id'];
    }

    public function getUrl()
    {
        return $this->getBaseWssUrl() . static::URL_PAGE;
    }

    public function getBaseWssUrl()
    {
        return $this->config->arExchangeInfo['base_wss_urls'][0];
    }

    public function getSendBeforeSubscriptions()
    {
        $arRequestBeforeSubscriptions = $this->getRequestBeforeSubscriptions();
        return $arRequestBeforeSubscriptions ? json_encode($arRequestBeforeSubscriptions) : null;
    }

    public function getRequestBeforeSubscriptions()
    {
        return null;
    }

    public function saveMessage($arMessage)
    {
        if (empty($arMessage)) {
            return false;
        }
        if (!$this->isMessageValid($arMessage)) {
            return false;
        }
        $arDataMessage = $this->getDataFromMessage($arMessage);

        return $this->saveCashSpotBook($arDataMessage);
    }

    public function saveManyMessages($arStoreMessage)
    {
        if (empty($arStoreMessage)) {
            return false;
        }
        $arDataMessages = [];
        foreach ($arStoreMessage as $arMessage) {
            // Валидацию мы прошли в $this->getSymbolFromMessage()
            $arDataMessages[] = $this->getDataFromMessage($arMessage);
        }

        return $this->saveCashSpotBookMany($arDataMessages);
    }

    public function isMessageValid(array $arMessage)
    {
        return false;
    }

    public function getSymbolFromMessage(array $arMessage)
    {
        return null;
    }

    public function getDataFromMessage(array $arMessage)
    {
        return [];
    }

    public function setMessageDataStoreBybit($message)
    {
        return false;
    }

    public function hasPing()
    {
        if (is_null(static::PING_TIME)) {
            return false;
        }
        if (is_null($this->flPingTimer)) {
            $this->flPingTimer = microtime(true);
        }
        if ((microtime(true) - $this->flPingTimer) > static::PING_TIME) {
            $this->flPingTimer = microtime(true);
            return true;
        }

        return false;
    }

    public function getPing($message)
    {
        return $this->getPingRequest($message) ? json_encode($this->getPingRequest($message)) : null;
    }

    public function getPingRequest($message)
    {
        return null;
    }

    public function isPingMessage($message, $bReturnRequest = true)
    {
        return false;
    }

    public function normalizeMessage($message)
    {
        return json_decode($message, true);
    }
}
