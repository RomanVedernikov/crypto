<?php

namespace Modules\Exchange\Services\WebSockets\Mexc;

use Illuminate\Support\Str;
use Modules\Exchange\Services\WebSockets\WsExchangeBase;
use Modules\Exchange\Entities\ExchangeInfo;


class SpotBook extends WsExchangeBase
{
    const EXCHANGE_CODE = 'mexc';
    const URL_PAGE = '';
    const PING_TIME = 20;
    // Всего ~??? символов
    const SUBSCRIPTIONS_ONE_SCRIPT = 30; // 30 символов в одном скрипте
    const SUBSCRIPTIONS_ONE_REQUEST = 10; // 10 символов в одном запросе

    public function __construct()
    {
        parent::__construct();
    }

    public function getRequestSubscription(array $arSubscriptions)
    {
        return [
            'method' => 'SUBSCRIPTION',
            'params' => $arSubscriptions,
            //'id' => time()
        ];
    }

    /**
     * <symbol>@bookTicker
     * <symbol>@depth5
     *
     * @param ExchangeInfo $obExchangeInfoSymbol
     * @return string
     */
    public function getSubscription(ExchangeInfo $obExchangeInfoSymbol)
    {
        return 'spot@public.limit.depth.v3.api@' . Str::upper($obExchangeInfoSymbol->symbol) . '@5';
    }

    public function getExchangeInfoSymbols()
    {
        return ExchangeInfo::where('exchange_id', $this->getExchangeId())
            ->where('is_spot_trading', true)
            ->orderBy('id')
            ->get();
    }

    public function getPingRequest($message)
    {
        return [
            'method' => 'PING',
            //'id' => time(),
        ];
    }

    public function isMessageValid($arMessage)
    {
        if (empty($arMessage['s']) || empty($arMessage['d'])) {
            return false;
        }
        return true;
    }

    public function getSymbolFromMessage(array $arMessage)
    {
        if (!$this->isMessageValid($arMessage)) {
            return null;
        }
        return $arMessage['s'] ?? null;
    }

    public function getDataFromMessage($arMessage)
    {
        $arData = [];
        foreach ($arMessage['d'] as $type => $arItems) {
            if ($type == 'bids' || $type == 'asks') {
                foreach ($arItems as $arItem) {
                    $arData[$type][] = [$arItem['p'], $arItem['v']];
                }
            }
        }
        return [
            'sSymbol' => $arMessage['s'],
            'arData' => $arData,
        ];
    }
}
