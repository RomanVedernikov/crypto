<?php

namespace Modules\Exchange\Services\WebSockets\Bybit;

use Illuminate\Support\Str;
use Modules\Exchange\Services\WebSockets\WsExchangeBase;
use Modules\Exchange\Entities\ExchangeInfo;

class SpotBook extends WsExchangeBase
{
    const EXCHANGE_CODE = 'bybit';
    const URL_PAGE = '/v5/public/spot';
    // Всего ~380 символов
    const SUBSCRIPTIONS_ONE_SCRIPT = 40;
    const SUBSCRIPTIONS_ONE_REQUEST = 10;

    public $arMessageDataStore;

    public function __construct()
    {
        parent::__construct();
    }

    public function getRequestSubscription(array $arSubscriptions)
    {
        return [
            'op' => 'subscribe',
            'args' => $arSubscriptions
        ];
    }

    public function getSubscription(ExchangeInfo $obExchangeInfoSymbol)
    {
        return 'orderbook.1.' . Str::upper($obExchangeInfoSymbol->symbol);
    }

    public function getExchangeInfoSymbols()
    {
        return ExchangeInfo::where('exchange_id', $this->getExchangeId())
            ->where('is_spot_trading', true)
            ->orderBy('id')
            ->get();
    }

    public function getPingRequest($message)
    {
        return [
            'op' => 'ping'
        ];
    }

    public function isMessageValid($arMessage)
    {
        if (empty($arMessage['type']) || empty($arMessage['data']) || empty($arMessage['topic'])) {
            return false;
        }
        return true;
    }

    public function getSymbolFromMessage(array $arMessage)
    {
        if (!$this->isMessageValid($arMessage)) {
            return null;
        }
        return $arMessage['data']['s'] ?? null;
    }

    public function getDataFromMessage($arMessage)
    {
        $arData = $this->getDataByTopic($arMessage['topic']);
        return [
            'sSymbol' => $arMessage['data']['s'],
            'arData' => $arData,
        ];
    }

    public function getDataByTopic($sTopic)
    {
        return $this->arMessageDataStore[$sTopic] ?? [];
    }

    public function setMessageDataStoreBybit($arMessage)
    {
        if (!$this->isMessageValid($arMessage)) {
            return false;
        }
        $arData = [];
        switch ($arMessage['type']) {
            case 'snapshot':
                $arData = [
                    'bids' => $arMessage['data']['b'],
                    'asks' => $arMessage['data']['a'],
                ];
                break;
            case 'delta':
                $arData = $this->getDataByTopic($arMessage['topic']);
                if ($arMessage['data']['b']) {
                    $arData['bids'] = $arMessage['data']['b'];
                }
                if ($arMessage['data']['a']) {
                    $arData['asks'] = $arMessage['data']['a'];
                }
                break;
        }
        if (empty($arData)) {
            return false;
        }
        $this->arMessageDataStore[$arMessage['topic']] = $arData;
    }
}
