<?php

namespace Modules\Exchange\Services\WebSockets\Exmo;

use Illuminate\Support\Str;
use Modules\Command\Entities\Command;
use Modules\Exchange\Services\WebSockets\WsExchangeBase;
use Modules\Exchange\Entities\ExchangeInfo;


class SpotBook extends WsExchangeBase
{
    const EXCHANGE_CODE = 'exmo';
    const URL_PAGE = '';
    const PING_TIME = null;
    // Всего ~200 символов
    const SUBSCRIPTIONS_ONE_SCRIPT = 500; // символов в одном скрипте
    const SUBSCRIPTIONS_ONE_REQUEST = 500; // символов в одном запросе

    public $iCountMessageStop = 0;

    public function __construct()
    {
        parent::__construct();
    }

    public function getRequestSubscription(array $arSubscriptions)
    {
        return [
            'method' => 'subscribe',
            'topics' => $arSubscriptions,
            'id' => time()
        ];
    }

    /**
     * <symbol>@bookTicker
     * <symbol>@depth5
     *
     * @param ExchangeInfo $obExchangeInfoSymbol
     * @return string
     */
    public function getSubscription(ExchangeInfo $obExchangeInfoSymbol)
    {
        return 'spot/order_book_snapshots:' . Str::upper($obExchangeInfoSymbol->base_asset) . '_' . Str::upper($obExchangeInfoSymbol->quote_asset);
    }

    public function getExchangeInfoSymbols()
    {
        return ExchangeInfo::where('exchange_id', $this->getExchangeId())
            ->where('is_spot_trading', true)
            ->orderBy('id')
            ->get();
    }

    public function isMessageValid($arMessage)
    {
        $this->checkMessageStopStream($arMessage);
        if (
            isset($arMessage['event']) &&
            $arMessage['event'] != 'update'
        ) {
            return false;
        }
        if (
            !isset($arMessage['topic']) ||
            !isset($arMessage['data'])
        ) {
            return false;
        }
        return true;
    }

    public function getSymbolFromMessage($arMessage)
    {
        if (!$this->isMessageValid($arMessage)) {
            dump($arMessage);
            return null;
        }
        list($var, $sSymbol) = explode(':', $arMessage['topic']);
        $sSymbol = str_replace('_', '', $sSymbol);
        return $sSymbol ?? null;
    }

    public function getDataFromMessage($arMessage)
    {
        list($var, $sSymbol) = explode(':', $arMessage['topic']);
        $sSymbol = str_replace('_', '', $sSymbol);
        if (empty($sSymbol)) {
            return null;
        }
        $arData = [];
        for ($i = 0; $i < count($arMessage['data']['bid']); $i++) {
            $arData['bids'][] = [(float)$arMessage['data']['bid'][$i][0], (float)$arMessage['data']['bid'][$i][1]];
            if ($i == 9) {
                break;
            }
        }
        for ($i = 0; $i < count($arMessage['data']['ask']); $i++) {
            $arData['asks'][] = [(float)$arMessage['data']['ask'][$i][0], (float)$arMessage['data']['ask'][$i][1]];
            if ($i == 9) {
                break;
            }
        }
        if (empty($arData)) {
            return null;
        }
        return [
            'sSymbol' => $sSymbol,
            'arData' => $arData,
        ];
    }

    /**
     * Exmo работает 30 сек, потом прилетет стоп сообщение, нужно написать в ТП
     * Пока просто перезапустим скрипт
     *
     * @param $arMessage
     */
    public function checkMessageStopStream($arMessage)
    {
        // connection established
        if (
            isset($arMessage['event']) &&
            $arMessage['event'] == 'info' &&
            isset($arMessage['code']) &&
            $arMessage['code'] == 1
        ) {

            $this->iCountMessageStop++;

            // Прилетает 2 сообщения: после подписки, и после 30 сек
            if ($this->iCountMessageStop >= 2) {
                // Просто выйдем, потому что CheckAndRun отрабатывает каждые 2 секунды!
                die();
            }
        }
    }

}
