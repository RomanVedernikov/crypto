<?php

namespace Modules\Exchange\Services\WebSockets\Kraken;

use Illuminate\Support\Str;
use Modules\Exchange\Services\WebSockets\WsExchangeBase;
use Modules\Exchange\Entities\ExchangeInfo;


class SpotBook extends WsExchangeBase
{
    const EXCHANGE_CODE = 'kraken';
    const URL_PAGE = '';
    const PING_TIME = 20;
    // Всего ~??? символов
    const SUBSCRIPTIONS_ONE_SCRIPT = 1; // 30 символов в одном скрипте
    const SUBSCRIPTIONS_ONE_REQUEST = 1; // 10 символов в одном запросе

    public function __construct()
    {
        parent::__construct();
    }

    public function getRequestSubscription(array $arSubscriptions)
    {
        return [
            'event' => 'subscribe',
            'pair' => $arSubscriptions,
            'subscription' => [
                'name' => 'book',
                'depth' => 10
            ]
        ];
    }

    /**
     * <symbol>@bookTicker
     * <symbol>@depth5
     *
     * @param ExchangeInfo $obExchangeInfoSymbol
     * @return string
     */
    public function getSubscription(ExchangeInfo $obExchangeInfoSymbol)
    {
        return $obExchangeInfoSymbol->ws_symbol;
    }

    public function getExchangeInfoSymbols()
    {
        return ExchangeInfo::where('exchange_id', $this->getExchangeId())
            ->where('is_spot_trading', true)
            ->orderBy('id')
            ->get();
    }

    public function getPingRequest($message)
    {
        return [
            'method' => 'PING',
            //'id' => time(),
        ];
    }

    public function isMessageValid($arMessage)
    {
        if (empty($arMessage['s']) || empty($arMessage['d'])) {
            return false;
        }
        return true;
    }

    public function getSymbolFromMessage(array $arMessage)
    {
        if (!$this->isMessageValid($arMessage)) {
            return null;
        }
        return $arMessage['s'] ?? null;
    }

    public function getDataFromMessage($arMessage)
    {
        $arData = [];
        foreach ($arMessage['d'] as $type => $arItems) {
            if ($type == 'bids' || $type == 'asks') {
                foreach ($arItems as $arItem) {
                    $arData[$type][] = [$arItem['p'], $arItem['v']];
                }
            }
        }
        return [
            'sSymbol' => $arMessage['s'],
            'arData' => $arData,
        ];
    }
}
