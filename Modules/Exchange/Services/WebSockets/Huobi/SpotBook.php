<?php

namespace Modules\Exchange\Services\WebSockets\Huobi;

use Illuminate\Support\Str;
use Modules\Exchange\Services\WebSockets\WsExchangeBase;
use Modules\Exchange\Entities\ExchangeInfo;


class SpotBook extends WsExchangeBase
{
    const EXCHANGE_CODE = 'huobi';
    const URL_PAGE = '';
    const PING_TIME = null;
    // Всего ~750 символов
    const SUBSCRIPTIONS_ONE_SCRIPT = 10; // 50
    const SUBSCRIPTIONS_ONE_REQUEST = 1;

    public function __construct()
    {
        parent::__construct();
    }

    public function getRequestSubscription(array $arSubscriptions)
    {
        return $arSubscriptions[0];
    }

    public function getSubscription(ExchangeInfo $obExchangeInfoSymbol)
    {
        return [
            "sub" => "market." . Str::lower($obExchangeInfoSymbol->symbol) . ".bbo",
            //"sub" => "market." . Str::lower($obExchangeInfoSymbol->symbol) . ".depth.step5",
            //"id" => time() . "" . $obExchangeInfoSymbol->id
        ];
    }

    public function getExchangeInfoSymbols()
    {
        return ExchangeInfo::where('exchange_id', $this->getExchangeId())
            ->where('is_spot_trading', true)
            ->orderBy('id')
            ->get();
    }

    public function isPingMessage($arMessage, $bReturnRequest = true)
    {
        if (empty($arMessage['ping'])) {
            return false;
        }
        if ($bReturnRequest) {
            return json_encode($this->getPingRequest($arMessage));
        }
        return true;
    }

    public function getPingRequest($arMessage)
    {
        return [
            'pong' => $arMessage['ping']
        ];
    }

    /**
     * Все возвращаемые данные API WebSocket Market сжаты с помощью GZIP, поэтому их необходимо распаковать.
     *
     * @param $message
     * @return mixed
     */
    public function normalizeMessage($message)
    {
        // local.ERROR: gzdecode(): data error (ErrorException)
        try {
            $message = gzdecode($message);
        } catch (\ErrorException $e) {
            return null;
        }
        return json_decode($message, true);
    }

    public function isMessageValid($arMessage)
    {
        if (empty($arMessage['tick']['symbol'])) {
            return false;
        }
        return true;
    }

    public function getSymbolFromMessage($arMessage)
    {
        if (!$this->isMessageValid($arMessage)) {
            //dump($arMessage);
            return null;
        }
        return $arMessage['tick']['symbol'] ?? null;
    }

    public function getDataFromMessage($arMessage)
    {
        return [
            'sSymbol' => $arMessage['tick']['symbol'],
            'arData' => [
                'bids' => [
                    [$arMessage['tick']['bid'], $arMessage['tick']['bidSize']]
                ],
                'asks' => [
                    [$arMessage['tick']['ask'], $arMessage['tick']['askSize']]
                ]
            ],
        ];
    }
}
