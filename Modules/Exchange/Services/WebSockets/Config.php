<?php

namespace Modules\Exchange\Services\WebSockets;


class Config
{

    const EXCHANGE_INFO = [
        'binance' => [
            'exchange_id' => 1,
            'base_wss_urls' => [
                'wss://stream.binance.com:443',
                'wss://stream.binance.com:9443',
                'wss://data-stream.binance.com',
            ],
        ],
        'bybit' => [
            'exchange_id' => 2,
            'base_wss_urls' => [
                'wss://stream.bybit.com',
            ],
        ],
        'huobi' => [
            'exchange_id' => 3,
            'base_wss_urls' => [
                //'wss://api.huobi.pro/feed',
                'wss://api.huobi.pro/ws',
                'wss://api-aws.huobi.pro/ws',
            ],
        ],
        'kucoin' => [
            'exchange_id' => 4,
            'base_wss_urls' => [
                'wss://ws-api-spot.kucoin.com',
            ],
        ],
        'mexc' => [
            'exchange_id' => 5,
            'base_wss_urls' => [
                'wss://wbs.mexc.com/ws',
            ],
        ],
        'gate' => [
            'exchange_id' => 6,
            'base_wss_urls' => [
                'wss://api.gateio.ws/ws/v4/',
            ],
        ],
        'exmo' => [
            'exchange_id' => 7,
            'base_wss_urls' => [
                'wss://ws-api.exmo.com:443/v1/public',
            ],
        ],
        'kraken' => [
            'exchange_id' => 8,
            'base_wss_urls' => [
                'wss://ws.kraken.com',
            ],
        ],
    ];

    public $arExchangeInfo;

    public function __construct(string $exchange_code)
    {
        $this->arExchangeInfo = static::EXCHANGE_INFO[$exchange_code];
    }

    public static function getListExchange()
    {
        return array_keys(static::EXCHANGE_INFO);
    }

    public static function getExchangeIdByCode($exchange_code)
    {
        return static::EXCHANGE_INFO[$exchange_code]['exchange_id'];
    }
}
