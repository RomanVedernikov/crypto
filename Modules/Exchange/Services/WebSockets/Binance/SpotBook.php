<?php

namespace Modules\Exchange\Services\WebSockets\Binance;

use Illuminate\Support\Str;
use Modules\Exchange\Services\WebSockets\WsExchangeBase;
use Modules\Exchange\Entities\ExchangeInfo;


class SpotBook extends WsExchangeBase
{
    const EXCHANGE_CODE = 'binance';
    const URL_PAGE = '/ws/bnbbtc@depth5';
    const PING_TIME = null;
    // Всего ~2172 символов
    const SUBSCRIPTIONS_ONE_SCRIPT = 300; // 300 символов в одном скрипте
    const SUBSCRIPTIONS_ONE_REQUEST = 100; // 100 символов в одном запросе

    public function __construct()
    {
        parent::__construct();
    }

    public function getRequestSubscription(array $arSubscriptions)
    {
        return [
            'method' => 'SUBSCRIBE',
            'params' => $arSubscriptions,
            'id' => time()
        ];
    }

    /**
     * <symbol>@bookTicker
     * <symbol>@depth5
     *
     * @param ExchangeInfo $obExchangeInfoSymbol
     * @return string
     */
    public function getSubscription(ExchangeInfo $obExchangeInfoSymbol)
    {
        return Str::lower($obExchangeInfoSymbol->symbol) . '@depth5';
    }

    public function getExchangeInfoSymbols()
    {
        return ExchangeInfo::where('exchange_id', $this->getExchangeId())
            ->where('is_spot_trading', true)
            ->orderBy('id')
            ->get();
    }

    public function getRequestBeforeSubscriptions()
    {
        return [
            'method' => 'SET_PROPERTY',
            'params' => [
                'combined', true
            ],
            'id' => time()
        ];
    }

    public function getPingRequest($message)
    {
        return [
            'method' => 'ping',
            'id' => time(),
        ];
    }

    public function isMessageValid($arMessage)
    {
        if (empty($arMessage['stream']) || empty($arMessage['data'])) {
            return false;
        }
        return true;
    }

    public function getSymbolFromMessage(array $arMessage)
    {
        if (!$this->isMessageValid($arMessage)) {
            return null;
        }
        $arData = explode('@', $arMessage['stream']);
        return $arData[0] ?? null;
    }

    public function getDataFromMessage($arMessage)
    {
        list($sSymbol, $sStream) = explode('@', $arMessage['stream']);
        if (empty($sSymbol)) {
            return null;
        }
        return [
            'sSymbol' => $sSymbol,
            'arData' => $arMessage['data'],
        ];
    }

}
