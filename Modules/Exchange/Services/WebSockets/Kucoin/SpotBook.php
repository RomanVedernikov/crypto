<?php

namespace Modules\Exchange\Services\WebSockets\Kucoin;

use Illuminate\Support\Str;
use Modules\Exchange\Services\WebSockets\WsExchangeBase;
use Modules\Exchange\Entities\ExchangeInfo;
use Modules\Exchange\Services\Api\Kucoin\Config as KucoinApiConfig;
use Modules\Exchange\Services\Api\Request as ApiRequest;

class SpotBook extends WsExchangeBase
{
    const EXCHANGE_CODE = 'kucoin';
    const URL_PAGE = '';
    const PING_TIME = 9; // sec
    // Всего ~1500 символов
    const SUBSCRIPTIONS_ONE_SCRIPT = 9999999; // все в одном скрипте!
    const SUBSCRIPTIONS_ONE_REQUEST = 5;

    public $sToken = null;
    public $sBaseUrl = null;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Отправьте запрос следующим образом, чтобы получить список серверов и временный общедоступный токен
     */
    public function sendBulletPublic()
    {
        $obResponse = ApiRequest::getMethod(KucoinApiConfig::BASE_API_URL, KucoinApiConfig::getMethodArray('bulletPublic'));
        if ($obResponse->status() != 200) {
            // Ошибка запроса
            dd($obResponse->status());
            return false;
        }
        $arResponse = $obResponse->collect()->get('data');
        //dd($arResponse);
        $this->sToken = $arResponse['token'];
        $this->sBaseUrl = $arResponse['instanceServers'][0]['endpoint'];

        return true;
    }

    public function getUrl()
    {
        $sBaseUrl = $this->sBaseUrl ?? $this->getBaseWssUrl();
        return $sBaseUrl . '?token=' . $this->sToken;
    }

    public function getRequestBeforeSubscriptions()
    {
        return [
            'type' => 'subscribe',
            'topic' => '/market/ticker:all',
            'id' => time()
        ];
    }

    public function getNextSubscription()
    {
        return false;
    }

    public function getPingRequest($message)
    {
        return [
            'type' => 'ping',
            'id' => (string)time(),
        ];
    }

    /**
     * /spotMarket/level2Depth5:{symbol},{symbol}...
     *
     */
    public function getRequestSubscription(array $arSubscriptions)
    {
        return [
            'type' => 'subscribe',
            'topic' => '/spotMarket/level2Depth5:' . implode(',', $arSubscriptions),
            'privateChannel' => false,
            'response' => true,
            'id' => time()
        ];
    }

    public function getSubscription(ExchangeInfo $obExchangeInfoSymbol)
    {
        return [$obExchangeInfoSymbol->base_asset . '-' . $obExchangeInfoSymbol->quote_asset];
    }

    public function getExchangeInfoSymbols()
    {
        return ExchangeInfo::where('exchange_id', $this->getExchangeId())
            ->where('is_spot_trading', true)
            ->orderBy('id')
            ->get();
    }

    public function isMessageValid($arMessage)
    {
        if (empty($arMessage['subject']) || empty($arMessage['data'])) {
            return false;
        }
        return true;
    }

    public function getSymbolFromMessage(array $arMessage)
    {
        if (!$this->isMessageValid($arMessage)) {
            return null;
        }
        return str_replace('-', '', $arMessage['subject']) ?? null;
    }

    public function getDataFromMessage($arMessage)
    {
        return [
            'sSymbol' => str_replace('-', '', $arMessage['subject']),
            'arData' => [
                'bids' => [
                    [$arMessage['data']['bestBid'], $arMessage['data']['bestBidSize']]
                ],
                'asks' => [
                    [$arMessage['data']['bestAsk'], $arMessage['data']['bestAskSize']]
                ],
                'price' => $arMessage['data']['price']
            ],
        ];
    }
}
