<?php

namespace Modules\Exchange\Services\WebSockets\Gate;

use Illuminate\Support\Str;
use Modules\Exchange\Services\WebSockets\WsExchangeBase;
use Modules\Exchange\Entities\ExchangeInfo;


class SpotBook extends WsExchangeBase
{
    const EXCHANGE_CODE = 'gate';
    const URL_PAGE = '';
    const PING_TIME = 20;
    // Всего ~??? символов
    const SUBSCRIPTIONS_ONE_SCRIPT = 300; // 30 символов в одном скрипте
    const SUBSCRIPTIONS_ONE_REQUEST = 300; // 10 символов в одном запросе

    public function __construct()
    {
        parent::__construct();
    }

    public function getRequestSubscription(array $arSubscriptions)
    {
        return [
            'channel' => 'spot.book_ticker', // spot.order_book
            'event' => 'subscribe',
            'payload' => $arSubscriptions,
            'time' => time()
        ];
    }

    /**
     * <symbol>@bookTicker
     * <symbol>@depth5
     *
     * @param ExchangeInfo $obExchangeInfoSymbol
     * @return string
     */
    public function getSubscription(ExchangeInfo $obExchangeInfoSymbol)
    {
        return Str::upper($obExchangeInfoSymbol->base_asset) . '_' . Str::upper($obExchangeInfoSymbol->quote_asset);
    }

    public function getExchangeInfoSymbols()
    {
        return ExchangeInfo::where('exchange_id', $this->getExchangeId())
            ->where('is_spot_trading', true)
            ->orderBy('id')
            ->get();
    }

    public function getPingRequest($message)
    {
        return [
            'channel' => 'spot.ping',
            'time' => time()
        ];
    }

    public function isMessageValid($arMessage)
    {
        if (
            isset($arMessage['event']) &&
            $arMessage['event'] != 'update'
        ) {
            return false;
        }
        if (
            isset($arMessage['channel']) &&
            $arMessage['channel'] != 'spot.book_ticker'
        ) {
            return false;
        }
        if (
            !isset($arMessage['result']) ||
            !isset($arMessage['result']['s']) ||
            !isset($arMessage['result']['b']) ||
            !isset($arMessage['result']['B']) ||
            !isset($arMessage['result']['a']) ||
            !isset($arMessage['result']['A'])
        ) {
            return false;
        }
        return true;
    }

    public function getSymbolFromMessage(array $arMessage)
    {
        if (!$this->isMessageValid($arMessage)) {
            return null;
        }
        return str_replace('_', '', $arMessage['result']['s']) ?? null;
    }

    public function getDataFromMessage($arMessage)
    {
        return [
            'sSymbol' => str_replace('_', '', $arMessage['result']['s']),
            'arData' => [
                'bids' => [
                    [(float)$arMessage['result']['b'], (float)$arMessage['result']['B']]
                ],
                'asks' => [
                    [(float)$arMessage['result']['a'], (float)$arMessage['result']['A']]
                ]
            ],
        ];
    }
}
