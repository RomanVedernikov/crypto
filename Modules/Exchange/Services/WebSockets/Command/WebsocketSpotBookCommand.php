<?php

namespace Modules\Exchange\Services\WebSockets\Command;

use Modules\Exchange\Services\WebSockets as WS;
use Modules\Command\Entities\Command as CommandEntity;
use Modules\Logger\Services\Logger;
use Symfony\Component\Console\Helper\ProgressBar;
use Laravel\Telescope\Telescope;

class WebsocketSpotBookCommand
{
    const EXCHANGE_CLASS = [
        'binance' => WS\Binance\SpotBook::class,
        'bybit' => WS\Bybit\SpotBook::class,
        'huobi' => WS\Huobi\SpotBook::class,
        'kucoin' => WS\Kucoin\SpotBook::class,
        'mexc' => WS\Mexc\SpotBook::class,
        'gate' => WS\Gate\SpotBook::class,
        'exmo' => WS\Exmo\SpotBook::class,
        //'kraken' => WS\Kraken\SpotBook::class,
    ];

    const TIME_STORE_MESSAGE = 3; // 1, 2, 3 sec
    const TIME_LOGGER = 600; // 60 * 10 min
    const TIME_RESTART = 3600; // 60 * 60 min (1 час)

    const PROGRESS_BAR_ENABLE = true;

    public $sExchangeCode;
    public $iScriptIndex;
    /** @var WS\WsExchangeBase */
    public $obSpotBook;
    /** @var \WebSocket\Client */
    public $obWsClient;
    /** @var array */
    // Накапливает сообщения и потом записывает разом
    public $arStoreMessage;
    // Таймеры
    public $iTimerStoreMessage;
    public $iTimerLogger;
    public $iTimerRestart;

    public $arArguments;
    public $arOptions;
    /** @var \Symfony\Component\Console\Input\InputInterface */
    public $obInput;
    /** @var \Symfony\Component\Console\Output\OutputInterface */
    public $obOutput;

    public function __construct(array $arOptions)
    {
        $this->arArguments = $arOptions['arguments'];
        $this->arOptions = $arOptions['options'];
        $this->obInput = $arOptions['input'];
        $this->obOutput = $arOptions['output'];
        $this->sExchangeCode = $this->arArguments['exchange_code'];
        $this->iScriptIndex = $this->arArguments['script_index'];
        $this->obSpotBook = $this->getClass();
    }

    public function run()
    {

        Telescope::stopRecording();

        $this->saveCommand();

        if (is_null($this->obSpotBook)) {
            return null;
        }
        if ($this->sExchangeCode == 'kucoin') {
            if (!$this->obSpotBook->sendBulletPublic()) {
                return 0;
            }
        }
        $this->connectWsClient();
        if (is_null($this->obWsClient)) {
            return null;
        }
        // Сделаем подписки к Бирже
        $this->sendSubscribes();
        // Почистим память
        $this->clearMemory();

        //dd(1);

        // Выведем progressBar в консоли
        if ($this->isProgressBarEnable()) {
            $progressBar = new ProgressBar($this->obOutput);
            $progressBar->setFormat('debug');
            $progressBar->start();
        }

        $this->startTimerStoreMessage();
        $this->iTimerLogger = microtime(true);
        $this->iTimerRestart = microtime(true);

        while (true) {

            // Ждем сообщение из Сокет-Клиента
            try {
                $message = $this->obWsClient->receive();
                $message = $this->obSpotBook->normalizeMessage($message);

                $this->processMessage($message);
                $this->processPingMessage($message);

            } catch (\WebSocket\ConnectionException $e) {
                dump("Error: " . $e->getMessage());
            }

            // Если нужен Ping по таймеру
            $this->hasPing();

            // Запись в лог по таймеру
            $this->writeLogAtTimer();

            // Перезапустить команду по таймеру
            $this->restartCommandAtTimer();

            // Выводим лог в консоли
            if ($this->isProgressBarEnable()) {
                $progressBar->advance();
            }
        }

        $this->obWsClient->close();

        return 0;
    }

    private function processMessage($message)
    {
        dump($message);

        // Для Bybit мы должны все сообщения обрабатывать, потому что там есть сообщения snapshot и delta
        if ($this->sExchangeCode == 'bybit') {
            $this->obSpotBook->setMessageDataStoreBybit($message);
        }

        // Чтобы снизить нагрузку на процессор, будем копить сообщения и записывать раз в 1-2-3 сек
        if (static::TIME_STORE_MESSAGE > 0) {
            // Сохраним сообщение в массив
            if ($sSymbol = $this->obSpotBook->getSymbolFromMessage($message)) {
                $this->arStoreMessage[$sSymbol] = $message;
            }
            // Если время пришло
            if ($this->isTimePassedForStoreMessage() && !empty($this->arStoreMessage)) {
                dump(count($this->arStoreMessage));
                // Запишем все сообщения в кеш
                $this->obSpotBook->saveManyMessages($this->arStoreMessage);
                // Почистим массив и сбросим таймер
                $this->arStoreMessage = [];
                $this->startTimerStoreMessage();
            }
        } else {
            // Сразу запишем сообщение в кеш
            $bSave = $this->obSpotBook->saveMessage($message);
        }
    }

    private function processPingMessage($message)
    {
        // Некоторые биржи шлют ping и нужно ответить
        if ($sPong = $this->obSpotBook->isPingMessage($message, true)) {
            //dump($sPong);
            $this->obWsClient->text($sPong);
        }
    }

    private function sendSubscribes()
    {
        // При подключении к Kucoin нужно подождать ответ "welcome"
        if ($this->sExchangeCode == 'kucoin') {
            $message = $this->obWsClient->receive();
            $message = $this->obSpotBook->normalizeMessage($message);
            //dump($message);
        }

        // Запрос сервису перед подпиской, настройки например
        if ($sSendBeforeSubscriptionsText = $this->obSpotBook->getSendBeforeSubscriptions()) {
            // Запрос
            //dump($sSendBeforeSubscriptionsText);
            $this->obWsClient->text($sSendBeforeSubscriptionsText);
        }

        /**
         * Подписки сгруппированы по:
         * 1. Кол-во скриптов
         * 2. Кол-во подписок в одном запросе
         */
        $this->obSpotBook->setScriptIndex($this->iScriptIndex);
        while ($sSubscribText = $this->obSpotBook->getNextSubscription()) {
            // Запрос
            dump($sSubscribText);
            $this->obWsClient->text($sSubscribText);
            // Обработка ответа
        }
    }

    private function hasPing()
    {
        if ($this->obSpotBook->hasPing()) {
            if ($sPing = $this->obSpotBook->getPing([])) {
                //dump($sPing);
                $this->obWsClient->text($sPing);
            } else {
                $this->obWsClient->ping();
            }
        }
    }

    private function saveCommand()
    {
        $this->obSpotBook->obCommandEntity = CommandEntity::create([
            'pid' => getmypid(),
            'command' => 'ws:spot-book ' . $this->sExchangeCode . ' ' . $this->iScriptIndex,
            'signature' => 'ws:spot-book {exchange_code} {script_index?}',
        ]);
    }

    private function connectWsClient()
    {
        if (is_null($this->obSpotBook)) {
            return null;
        }
        $this->obWsClient = new \WebSocket\Client($this->obSpotBook->getUrl());
    }

    private function getClass()
    {
        $sClassName = static::EXCHANGE_CLASS[$this->sExchangeCode];
        if ($sClassName && class_exists($sClassName)) {
            return new $sClassName();
        }
        return null;
    }

    private function clearMemory()
    {
        // Очищаем переменные
        $this->obSpotBook->arSubscriptions = null;
        $this->obSpotBook->arGroupSubscriptions = null;

        // Отключим DB
        \DB::disconnect();
    }

    private function isProgressBarEnable()
    {
        return static::PROGRESS_BAR_ENABLE === true;
    }

    public function startTimerStoreMessage()
    {
        if (static::TIME_STORE_MESSAGE > 0) {
            $this->iTimerStoreMessage = microtime(true);
        }
    }

    public function isTimePassedForStoreMessage()
    {
        if (
            static::TIME_STORE_MESSAGE > 0 &&
            (microtime(true) - $this->iTimerStoreMessage) >= static::TIME_STORE_MESSAGE
        ) {
            return true;
        }
        return false;
    }

    public function registerShutdown()
    {
        $error = error_get_last();
        dump([1, $error]);
        Logger::write("Команда завершилась с ошибкой: {$error}");
    }

    public function writeLogAtTimer()
    {
        if ((microtime(true) - $this->iTimerLogger) < static::TIME_LOGGER) { // 10 мин
            return false;
        }
        $this->iTimerLogger = microtime(true);

        Logger::write('Команда: ' . $this->obSpotBook->obCommandEntity->command);
        Logger::write("Память скрипта: " . round(memory_get_usage() / (1024 * 1024), 1) . " Mb");
    }

    public function restartCommandAtTimer()
    {
        if ((microtime(true) - $this->iTimerRestart) < static::TIME_RESTART) { // 1 час
            return false;
        }
        // Если мы копим сообщения, то перезапустим когда запишем очередную порцию
        if (
            static::TIME_STORE_MESSAGE > 0 &&
            empty($this->arStoreMessage)
        ) {
            $this->dieScript();
        }
        // Если не копим сообщения, то сразу перезапустим
        if (is_null(static::TIME_STORE_MESSAGE)) {
            $this->dieScript();
        }
    }

    public function dieScript()
    {
        Logger::write('Отключил команду по таймеру: ' . $this->obSpotBook->obCommandEntity->command);
        die();
    }
}
