<?php

namespace Modules\Exchange\Services\WebSockets\Command;

use Modules\Exchange\Services\Command\CheckAndRun;
use Modules\Exchange\Services\WebSockets\Command\WebsocketSpotBookCommand;
use Modules\Exchange\Services\Tools\Terminal;
use Modules\Exchange\Services\WebSockets\Config;
use Modules\Exchange\Services\WebSockets\WsExchangeBase;
use Modules\Command\Entities\Command as CommandEntity;
use Illuminate\Support\Facades\Storage;

class WebsocketRunCommands
{

    public function __construct()
    {

    }

    public function run()
    {
        // Завершение команд
        $this->runKillComands();
        sleep(20);

        // Очистка таблицы команд Command
        CommandEntity::deleteAll();

        $this->runSpotBookCommands();

        $this->runSpreadHistoryComand();

        $this->runCheckAndRun();
    }

    public function runKillComands()
    {
        $sCommand = "php artisan command:kill-all";
        dump($sCommand);
        Terminal::startBackgroundProcess($sCommand);
    }

    public function runSpotBookCommands()
    {
        // Очиста Кеша SpotBook из Redis
        $this->clearCash(WsExchangeBase::CASH_KEY_PREFIX_SPOT_BOOK);

        foreach (WebsocketSpotBookCommand::EXCHANGE_CLASS as $sExchangeCode => $sExchangeClass) {
            if (
                class_exists($sExchangeClass) &&
                method_exists($sExchangeClass, 'getSubscriptions') &&
                $arGroupSubscriptions = (new $sExchangeClass())->getGroupSubscriptions()
            ) {
                for ($i = 0; $i < count(array_keys($arGroupSubscriptions)); $i++) {
                    $sCommand = "php artisan ws:spot-book {$sExchangeCode} {$i}";
                    dump($sCommand);
                    Terminal::startBackgroundProcess($sCommand);
                    sleep(1);
                }
            }
        }
    }

    public function runSpreadHistoryComand()
    {
        $sCommand = "php artisan crypto:spred-history";
        dump($sCommand);
        Terminal::startBackgroundProcess($sCommand);
    }

    public function runCheckAndRun()
    {
        if ($this->checkAlreadyProcessCommandCheckAndRun()) {
            return false;
        }
        $sCommand = "php artisan command:check-and-run";
        dump($sCommand);
        Terminal::startBackgroundProcess($sCommand);
    }

    public function checkAlreadyProcessCommandCheckAndRun()
    {
        // ToDo: нужно сделать не через файл, а через checkProcessByName - пока она не работает нормально!
        //Сделаем пока через файл
        if (Terminal::checkProcessByPid(CheckAndRun::getCommandPid())) {
            return true;
        }
        return false;
    }

    public function clearCash($prefix)
    {
        $hKeys = [];
        foreach (Config::getListExchange() as $sExchange) {
            $hKeys[] = "{$prefix}:{$sExchange}";
        }
        $res = \Redis::del($hKeys);
        return $res > 0;
    }
}
