<?php

namespace Modules\Exchange\Services\Spread;

use Modules\Exchange\Entities\ExchangeInfo;
use Modules\Exchange\Services\WebSockets\Command\WebsocketSpotBookCommand;
use Modules\Exchange\Services\WebSockets\Config as WebSocketsConfig;
use Modules\Exchange\Services\WebSockets\WsExchangeBase;

class Calculate
{
    public function __construct()
    {
    }

    public static function showPage()
    {
        $arSpotBooks = self::getResult();
        //dd($arSpotBooks['GRAILUSDT']);
        $obSpotBooks = collect($arSpotBooks)->sortByDesc('spread_truncate');
        foreach ($obSpotBooks as $symbol => $arSpotBook) {
            //dd($arSpotBook);
            $res = [];
            if (array_key_exists('spread_truncate', $arSpotBook)) {
                $res[] = $arSpotBook['spread_truncate'];
            }
            $res[] = $symbol;
            if (array_key_exists('arBestDataBuyResult', $arSpotBook) && array_key_exists('arBestDataSellResult', $arSpotBook)) {
                $res[] = $arSpotBook['arBestDataBuyResult']['exchange'] . ' -> ' . $arSpotBook['arBestDataSellResult']['exchange'];
            }
            if (array_key_exists('spread_truncate', $arSpotBook) && $arSpotBook['spread_truncate'] > 0) {
                $res[] = 'Цена: ' . $arSpotBook['arBestDataBuyResult']['price'] . ' -> ' . $arSpotBook['arBestDataSellResult']['price'];
            }
            if (array_key_exists('volume_buy', $arSpotBook)) {
                $res[] = 'Объем: ' . $arSpotBook['volume_buy'];
            }
            if (array_key_exists('profit', $arSpotBook)) {
                $res[] = 'Профит: ' . $arSpotBook['profit_truncate'];
            }
            echo implode(' / ', $res) . '</br>';
        }
    }

    public static function run()
    {
        return self::getResult();
    }

    public static function getResult()
    {
        $arSpotBooks = self::getAllSpotBooks();
        $arSpotBooks = self::prepareSpotBooks($arSpotBooks);
        $arSpotBooks = self::getSpotBooksBestPrice($arSpotBooks);
        $arSpotBooks = self::getSpotBooksSpread($arSpotBooks);
        $arSpotBooks = self::getSpotBooksProfit($arSpotBooks);

        return $arSpotBooks;
    }

    public static function getSpotBooksProfit($arSpotBooks)
    {
        foreach ($arSpotBooks as $symbol => &$arSpotBook) {

            if (!isset($arSpotBook['spread'])) {
                continue;
            }

            if (
                $arSpotBook['spread'] <= 0 ||
                $arSpotBook['spread'] > 1000
            ) {
                continue;
            }

            $flCommission = self::getCommission($arSpotBook);

            $arDataBuy = $arSpotBook['arBestDataBuyResult'];
            $arDataSell = $arSpotBook['arBestDataSellResult'];

            // Объем берем меньший из двух
            $flVolumeBuy = $arDataBuy['volume'] > $arDataSell['volume'] ? $arDataSell['volume'] : $arDataBuy['volume'];
            // Профит
            $flProfit = $arDataSell['price'] * $flVolumeBuy - $arDataBuy['price'] * $flVolumeBuy;
            // Профит минус процент комиссии
            $flProfit -= $flProfit * ($flCommission / 100);

            $arSpotBook['volume_buy'] = $flVolumeBuy;
            $arSpotBook['profit'] = $flProfit;
            $arSpotBook['profit_truncate'] = self::getTruncateNumber($flProfit);

        }
        return $arSpotBooks;
    }

    public static function getSpotBooksSpread($arSpotBooks)
    {
        foreach ($arSpotBooks as $symbol => &$arSpotBook) {

            //dd($arSpotBook);

            // Лучшая цена покупки
            $arDataBuy = $arSpotBook['arBestDataBuy'][0] ?? [];
            // Лучшая цена продажи
            $arDataSell = $arDataBuy['exchange'] == $arSpotBook['arBestDataSell'][0]['exchange'] ?
                $arSpotBook['arBestDataSell'][1] ?? [] :
                $arSpotBook['arBestDataSell'][0] ?? [];

            if (empty($arDataBuy) || empty($arDataSell)) {
                continue;
            }

            $arSpotBook['arBestDataBuyResult'] = $arDataBuy;
            $arSpotBook['arBestDataSellResult'] = $arDataSell;

            $arSpotBook['exchange_buy'] = WebSocketsConfig::getExchangeIdByCode($arDataBuy['exchange']);
            $arSpotBook['exchange_sell'] = WebSocketsConfig::getExchangeIdByCode($arDataSell['exchange']);

            $flCommission = self::getCommission($arSpotBook);
            $flSpread = 100 * ($arDataSell['price'] - $arDataBuy['price']) / $arDataBuy['price'];
            $flSpread -= $flCommission;
            $arSpotBook['spread'] = $flSpread;
            $arSpotBook['spread_truncate'] = self::getTruncateNumber($flSpread);

        }
        return $arSpotBooks;
    }

    /**
     * Более сложный рассчет можно сделать
     * Пока вернем просто -0,2%
     *
     * @param $arSpotBook
     * @return float
     */
    public static function getCommission($arSpotBook)
    {
        return 0.2;
    }

    public static function getTruncateNumber($number, $precision = 1)
    {
        if (!$number) {
            return null;
        }
        if (abs($number) < 0.001) {
            return round($number, 3);
        }
        return (float)round($number, $precision, PHP_ROUND_HALF_DOWN);
        //return (float)bcdiv($number, 1, $precision);
    }

    public static function getSpotBooksBestPrice($arSpotBooks)
    {
        $arResult = [];
        foreach ($arSpotBooks as $symbol => $arSpotBook) {
            if (count($arSpotBook) < 2) {
                continue;
            }
            $arDataBuy = $arDataSell = [];
            foreach ($arSpotBook as $exchange => $arData) {
                if (
                    isset($arData['buy']) &&
                    isset($arData['buy'][0][0]) &&
                    isset($arData['buy'][0][1]) &&
                    (float)$arData['buy'][0][0] > 0 &&
                    (float)$arData['buy'][0][1] > 0
                ) {
                    $arDataBuy[] = [
                        'price' => (float)$arData['buy'][0][0],
                        'volume' => (float)$arData['buy'][0][1],
                        'exchange' => $exchange
                    ];
                }
                if (
                    isset($arData['sell']) &&
                    isset($arData['sell'][0][0]) &&
                    isset($arData['sell'][0][1]) &&
                    (float)$arData['sell'][0][0] > 0 &&
                    (float)$arData['sell'][0][1] > 0
                ) {
                    $arDataSell[] = [
                        'price' => (float)$arData['sell'][0][0],
                        'volume' => (float)$arData['sell'][0][1],
                        'exchange' => $exchange
                    ];
                }
            }
            if ($arDataBuy) {
                $arDataBuy = collect($arDataBuy)->sortBy('price')->values()->toArray();
            }
            if ($arDataSell) {
                $arDataSell = collect($arDataSell)->sortByDesc('price')->values()->toArray();
            }

            $arSpotBook = array_merge($arSpotBook, [
                'arBestDataBuy' => $arDataBuy,
                'arBestDataSell' => $arDataSell
            ]);
            $arResult[$symbol] = $arSpotBook;
        }
        return $arResult;
    }

    public static function prepareSpotBooks($arSpotBooks)
    {
        $arResult = [];
        foreach ($arSpotBooks as $exchange => $arSymbols) {
            foreach ($arSymbols as $symbol => $jsonData) {
                $arData = json_decode($jsonData, 1);
                $arResult[$symbol][$exchange] = [
                    'buy' => $arData['asks'] ?? [],
                    'sell' => $arData['bids'] ?? []
                ];
            }
        }
        return $arResult;
    }

    public static function getAllSpotBooks()
    {
        $arSpotBooks = [];
        foreach (self::getListExchange() as $sExchange) {
            $sHash = WsExchangeBase::CASH_KEY_PREFIX_SPOT_BOOK . ":" . $sExchange;

            // ToDo: Перенести все команды с Redis в отдельный класс

            $arSpotBooks[$sExchange] = \Redis::hGetAll($sHash);
        }
        return $arSpotBooks;
    }

    public static function getListExchange()
    {
        return \Modules\Exchange\Services\WebSockets\Config::getListExchange();
    }

    public static function getSymbols()
    {
        $obSymbols = ExchangeInfo::orderBy('symbol')
            ->where('is_spot_trading', true)
            ->distinct()
            ->pluck('symbol');
        $arSymbols = [];
        foreach ($obSymbols as $sSymbol) {
            $arSymbols[] = $sSymbol;
        }
        return $arSymbols;
    }

}
