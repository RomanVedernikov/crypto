<?php

namespace Modules\Exchange\Services\Api\Mexc;

use Modules\Exchange\Entities\ExchangeInfo;
use Modules\Exchange\Services\Api\Request;

class GetExchangeInfo
{

    public static function run()
    {
        // Делаем запрос
        $obResponse = Request::getMethod(Config::BASE_API_URL, Config::getMethodArray('exchangeInfo'));

        if ($obResponse->status() != 200) {
            // Ошибка запроса
            dd($obResponse->status());
        }

        //dd($obResponse->collect()->get('symbols'));

        // Вытаскиваем symbols
        $arSymbols = $obResponse->collect()->get('symbols');

        // Записываем в базу
        self::saveSymbols($arSymbols);
    }

    /**
     * @param array $arSymbols
     */
    public static function saveSymbols(array $arSymbols)
    {
        $arResult = [];
        foreach ($arSymbols as $arSymbol) {
            $arResult[] = self::prepareSymbolForSave($arSymbol);
        }
        //dd($arResult);
        // Сохранить или Обновить записи
        ExchangeInfo::upsert(
            $arResult,
            ['exchange_id', 'symbol'],
            array_diff((new ExchangeInfo)->getFillable(), ['exchange_id', 'symbol'])
        );
    }

    /**
     * @param array $arSymbol
     * @return array
     */
    public static function prepareSymbolForSave(array $arSymbol)
    {
        return [
            'exchange_id' => Config::EXCHANGE_ID,
            'symbol' => $arSymbol['symbol'],
            'is_spot_trading' => $arSymbol['isSpotTradingAllowed'],
            'base_asset' => $arSymbol['baseAsset'],
            'quote_asset' => $arSymbol['quoteAsset'],
            'status' => $arSymbol['status'],
            'base_asset_precision' => $arSymbol['baseAssetPrecision'],
            'quote_asset_precision' => $arSymbol['quoteAssetPrecision'],
            'order_types' => is_array($arSymbol['orderTypes']) ?
                implode('|', $arSymbol['orderTypes']) :
                $arSymbol['orderTypes'],
            'permissions' => is_array($arSymbol['permissions']) ?
                implode('|', $arSymbol['permissions']) :
                $arSymbol['permissions'],
        ];
    }
}
