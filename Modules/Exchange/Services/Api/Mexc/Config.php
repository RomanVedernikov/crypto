<?php

namespace Modules\Exchange\Services\Api\Mexc;


class Config
{
    const EXCHANGE_ID = 5;

    const BASE_API_URL = 'https://api.mexc.com';

    const METHODS = [
        'exchangeInfo' => [
            'url' => '/api/v3/exchangeInfo',
            'method' => 'get',
        ],
    ];

    /**
     * @param string $sMethod
     * @return array|string[]
     */
    public static function getMethodArray(string $sMethod)
    {
        return static::METHODS[$sMethod] ?? [];
    }
}
