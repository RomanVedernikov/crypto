<?php

namespace Modules\Exchange\Services\Api\Binance;


class Config
{
    const EXCHANGE_ID = 1;

    const BASE_API_URL = 'https://api.binance.com';

    const PERMISSIONS = [
        "SPOT",
        "MARGIN",
        "LEVERAGED",
        "TRD_GRP_002",
        "TRD_GRP_003",
        "TRD_GRP_004",
        "TRD_GRP_005",
        "TRD_GRP_006",
        "TRD_GRP_007"
    ];

    const METHODS = [
        'exchangeInfo' => [
            'url' => '/api/v3/exchangeInfo',
            'method' => 'get',
        ]
    ];

    /**
     * @param string $sMethod
     * @return array|string[]
     */
    public static function getMethodArray(string $sMethod)
    {
        return static::METHODS[$sMethod] ?? [];
    }
}
