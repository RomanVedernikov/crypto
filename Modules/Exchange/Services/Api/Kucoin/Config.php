<?php

namespace Modules\Exchange\Services\Api\Kucoin;


class Config
{
    const EXCHANGE_ID = 4;

    const BASE_API_URL = 'https://api.kucoin.com';

    const METHODS = [
        'exchangeInfo' => [
            'url' => '/api/v2/symbols',
            'method' => 'get',
        ],
        'bulletPublic' => [
            'url' => '/api/v1/bullet-public',
            'method' => 'post',
        ],
    ];

    /**
     * @param string $sMethod
     * @return array|string[]
     */
    public static function getMethodArray(string $sMethod)
    {
        return static::METHODS[$sMethod] ?? [];
    }
}
