<?php

namespace Modules\Exchange\Services\Api\Bybit;


class Config
{
    const EXCHANGE_ID = 2;

    const BASE_API_URL = 'https://api.bybit.com';

    const METHODS = [
        'exchangeInfo' => [
            'url' => '/v5/market/instruments-info?category=spot',
            'method' => 'get',
        ]
    ];

    /**
     * @param string $sMethod
     * @return array|string[]
     */
    public static function getMethodArray(string $sMethod)
    {
        return static::METHODS[$sMethod] ?? [];
    }
}
