<?php

namespace Modules\Exchange\Services\Api\Bybit;

use Modules\Exchange\Entities\ExchangeInfo;
use Modules\Exchange\Services\Api\Request;

class GetExchangeInfo
{

    public static function run()
    {
        // Делаем запрос
        $obResponse = Request::getMethod(Config::BASE_API_URL, Config::getMethodArray('exchangeInfo'));

        if ($obResponse->status() != 200) {
            // Ошибка запроса
            dd($obResponse->status());
        }

        // Вытаскиваем symbols
        $arSymbols = $obResponse->collect()->get('result')['list'];

        //dd($arSymbols[0]);

        // Записываем в базу
        self::saveSymbols($arSymbols);
    }

    /**
     * @param array $arSymbols
     */
    public static function saveSymbols(array $arSymbols)
    {
        $arResult = [];
        foreach ($arSymbols as $arSymbol) {
            $arResult[] = self::prepareSymbolForSave($arSymbol);
        }
        // Сохранить или Обновить записи
        ExchangeInfo::upsert(
            $arResult,
            ['exchange_id', 'symbol'],
            array_diff((new ExchangeInfo)->getFillable(), ['exchange_id', 'symbol'])
        );
    }

    /**
     * @param array $arSymbol
     * @return array
     */
    public static function prepareSymbolForSave(array $arSymbol)
    {
        return [
            'exchange_id' => Config::EXCHANGE_ID,
            'symbol' => $arSymbol['symbol'],
            'status' => is_array($arSymbol['status']) ?
                implode('|', $arSymbol['status']) :
                $arSymbol['status'],
            'is_spot_trading' => ($arSymbol['status'] == 'Trading') ? true : false,
            'base_asset' => $arSymbol['baseCoin'],
            'quote_asset' => $arSymbol['quoteCoin'],
            'base_asset_precision' => null,
            'quote_asset_precision' => null,
            'order_types' => null,
            'permissions' => null,
        ];
    }
}
