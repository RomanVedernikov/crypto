<?php

namespace Modules\Exchange\Services\Api;

use Illuminate\Support\Facades\Http;


class Request
{
    public static function getMethod($sBaseApiUrl = '', $arMethod = [], $params = null)
    {
        if (!$arMethod) {
            return null;
        }
        $arMethod['base_url'] = $sBaseApiUrl;
        $arMethod['method_url'] = $arMethod['url'];
        $arMethod['url'] = $arMethod['base_url'] . $arMethod['method_url'];

        return self::makeRequest($arMethod, $params);
    }

    public static function makeRequest($arMethod = [], $mxParams = null)
    {
        if ($arMethod['method'] == 'get') {
            $obResponse = Http::get($arMethod['url'], $mxParams);
        }
        if ($arMethod['method'] == 'post') {
            $obResponse = Http::post($arMethod['url'], $mxParams ?? []);
        }

        return $obResponse;
    }
}
