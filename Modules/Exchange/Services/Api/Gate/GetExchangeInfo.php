<?php

namespace Modules\Exchange\Services\Api\Gate;

use Modules\Exchange\Entities\ExchangeInfo;
use Modules\Exchange\Services\Api\Request;

class GetExchangeInfo
{

    public static function run()
    {
        // Делаем запрос
        $obResponse = Request::getMethod(Config::BASE_API_URL, Config::getMethodArray('exchangeInfo'));

        if ($obResponse->status() != 200) {
            // Ошибка запроса
            dd($obResponse->status());
        }

        //dd($obResponse->collect()->get(1));

        // Вытаскиваем symbols
        $arSymbols = $obResponse->collect();

        // Записываем в базу
        self::saveSymbols($arSymbols);
    }

    /***
     * @param $arSymbols
     */
    public static function saveSymbols($arSymbols)
    {
        $arResult = [];
        foreach ($arSymbols as $arSymbol) {
            $arResult[] = self::prepareSymbolForSave($arSymbol);
        }
        //dd($arResult);
        // Сохранить или Обновить записи
        ExchangeInfo::upsert(
            $arResult,
            ['exchange_id', 'symbol'],
            array_diff((new ExchangeInfo)->getFillable(), ['exchange_id', 'symbol'])
        );
    }

    /**
     * @param $arSymbol
     * @return array
     */
    public static function prepareSymbolForSave($arSymbol)
    {
        return [
            'exchange_id' => Config::EXCHANGE_ID,
            'symbol' => $arSymbol['base'] . $arSymbol['quote'],
            'is_spot_trading' => $arSymbol['trade_status'] == 'tradable',
            'base_asset' => $arSymbol['base'],
            'quote_asset' => $arSymbol['quote'],
            'status' => $arSymbol['trade_status'],
            'base_asset_precision' => $arSymbol['precision'],
            'quote_asset_precision' => $arSymbol['precision'],
            'order_types' => null,
            'permissions' => null,
        ];
    }
}
