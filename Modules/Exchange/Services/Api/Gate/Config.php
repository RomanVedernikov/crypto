<?php

namespace Modules\Exchange\Services\Api\Gate;


class Config
{
    const EXCHANGE_ID = 6;

    const BASE_API_URL = 'https://api.gateio.ws/api/v4';

    const METHODS = [
        'exchangeInfo' => [
            'url' => '/spot/currency_pairs',
            'method' => 'get',
        ],
    ];

    /**
     * @param string $sMethod
     * @return array|string[]
     */
    public static function getMethodArray(string $sMethod)
    {
        return static::METHODS[$sMethod] ?? [];
    }
}
