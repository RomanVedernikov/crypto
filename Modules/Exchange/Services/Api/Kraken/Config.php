<?php

namespace Modules\Exchange\Services\Api\Kraken;


class Config
{
    const EXCHANGE_ID = 8;

    const BASE_API_URL = 'https://api.kraken.com/0';

    const METHODS = [
        'exchangeInfo' => [
            'url' => '/public/AssetPairs',
            'method' => 'get',
        ],
    ];

    /**
     * @param string $sMethod
     * @return array|string[]
     */
    public static function getMethodArray(string $sMethod)
    {
        return static::METHODS[$sMethod] ?? [];
    }
}
