<?php

namespace Modules\Exchange\Services\Api\Huobi;

use Modules\Exchange\Entities\ExchangeInfo;
use Modules\Exchange\Services\Api\Request;

class GetExchangeInfo
{

    public static function run()
    {
        // Делаем запрос
        $obResponse = Request::getMethod(Config::BASE_API_URL, Config::getMethodArray('exchangeInfo'));

        if ($obResponse->status() != 200) {
            // Ошибка запроса
            dd($obResponse->status());
        }

        //dd($obResponse->collect()->get('data')[0]);

        // Вытаскиваем symbols
        $arSymbols = $obResponse->collect()->get('data');

        // Записываем в базу
        self::saveSymbols($arSymbols);
    }

    /**
     * @param array $arSymbols
     */
    public static function saveSymbols(array $arSymbols)
    {
        $arResult = [];
        foreach ($arSymbols as $arSymbol) {
            $arResult[] = self::prepareSymbolForSave($arSymbol);
        }
        //dd($arResult);
        // Сохранить или Обновить записи
        ExchangeInfo::upsert(
            $arResult,
            ['exchange_id', 'symbol'],
            array_diff((new ExchangeInfo)->getFillable(), ['exchange_id', 'symbol'])
        );
    }

    /**
     * @param array $arSymbol
     * @return array
     */
    public static function prepareSymbolForSave(array $arSymbol)
    {
        return [
            'exchange_id' => Config::EXCHANGE_ID,
            'symbol' => $arSymbol['bcdn'] . $arSymbol['qcdn'],
            'status' => $arSymbol['state'],
            'is_spot_trading' => $arSymbol['te'],
            'base_asset' => $arSymbol['bcdn'],
            'quote_asset' => $arSymbol['qcdn'],
            'base_asset_precision' => null,
            'quote_asset_precision' => null,
            'order_types' => null,
            'permissions' => null,
        ];
    }
}
