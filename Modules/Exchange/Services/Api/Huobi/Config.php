<?php

namespace Modules\Exchange\Services\Api\Huobi;


class Config
{
    const EXCHANGE_ID = 3;

    const BASE_API_URL = 'https://api-aws.huobi.pro';

    const METHODS = [
        'exchangeInfo' => [
            'url' => '/v2/settings/common/symbols',
            'method' => 'get',
        ]
    ];

    /**
     * @param string $sMethod
     * @return array|string[]
     */
    public static function getMethodArray(string $sMethod)
    {
        return static::METHODS[$sMethod] ?? [];
    }
}
