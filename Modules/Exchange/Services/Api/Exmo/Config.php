<?php

namespace Modules\Exchange\Services\Api\Exmo;


class Config
{
    const EXCHANGE_ID = 7;

    const BASE_API_URL = 'https://api.exmo.com/v1.1';

    const METHODS = [
        'exchangeInfo' => [
            'url' => '/pair_settings',
            'method' => 'get',
        ],
    ];

    /**
     * @param string $sMethod
     * @return array|string[]
     */
    public static function getMethodArray(string $sMethod)
    {
        return static::METHODS[$sMethod] ?? [];
    }
}
