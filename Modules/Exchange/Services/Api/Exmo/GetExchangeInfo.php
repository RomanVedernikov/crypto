<?php

namespace Modules\Exchange\Services\Api\Exmo;

use Modules\Exchange\Entities\ExchangeInfo;
use Modules\Exchange\Services\Api\Request;

class GetExchangeInfo
{

    public static function run()
    {
        // Делаем запрос
        $obResponse = Request::getMethod(Config::BASE_API_URL, Config::getMethodArray('exchangeInfo'));

        if ($obResponse->status() != 200) {
            // Ошибка запроса
            dd($obResponse->status());
        }

        //dd($obResponse->collect());

        // Вытаскиваем symbols
        $arSymbols = $obResponse->collect()->toArray();

        // Записываем в базу
        self::saveSymbols($arSymbols);
    }

    /**
     * @param array $arSymbols
     */
    public static function saveSymbols(array $arSymbols)
    {
        $arResult = [];
        foreach ($arSymbols as $sSymbol => $arSymbol) {
            $arResult[] = self::prepareSymbolForSave($arSymbol, $sSymbol);
        }
        //dd($arResult);
        // Сохранить или Обновить записи
        ExchangeInfo::upsert(
            $arResult,
            ['exchange_id', 'symbol'],
            array_diff((new ExchangeInfo)->getFillable(), ['exchange_id', 'symbol'])
        );
    }

    /**
     * @param array $arSymbol
     * @return array
     */
    public static function prepareSymbolForSave(array $arSymbol, string $sSymbol)
    {
        $arAsset = explode('_', $sSymbol);
        return [
            'exchange_id' => Config::EXCHANGE_ID,
            'symbol' => $arAsset[0] . $arAsset[1],
            'is_spot_trading' => true,
            'base_asset' => $arAsset[0],
            'quote_asset' => $arAsset[1],
            'status' => null,
            'base_asset_precision' => null,
            'quote_asset_precision' => null,
            'order_types' => null,
            'permissions' => null,
        ];
    }
}
