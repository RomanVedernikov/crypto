<?php

namespace Modules\Exchange\Services;

use Illuminate\Support\Facades\Storage;
use Modules\Command\Entities\Command as CommandEntity;
use Modules\Exchange\Services\Command\CheckAndRun;
use Modules\Exchange\Services\Tools\Redis;
use Modules\Exchange\Services\WebSockets\Config;
use Modules\Logger\Services\Logger;
use Modules\SpreadHistory\Services\SpreadHistoryFillTable;

class Test
{

    public static function test()
    {
        set_time_limit(600);

        //self::GetExchangeInfo();

        \Modules\Exchange\Services\Spread\Calculate::showPage();

//        $obFillTable = new SpreadHistoryFillTable();
//        $obFillTable->run();

        dd('end');
    }

    public static function GetExchangeInfo()
    {
        \Modules\Exchange\Services\Api\Binance\GetExchangeInfo::run();
        \Modules\Exchange\Services\Api\Bybit\GetExchangeInfo::run();
        \Modules\Exchange\Services\Api\Huobi\GetExchangeInfo::run();
        \Modules\Exchange\Services\Api\Kucoin\GetExchangeInfo::run();
        \Modules\Exchange\Services\Api\Mexc\GetExchangeInfo::run();
        \Modules\Exchange\Services\Api\Gate\GetExchangeInfo::run();
        \Modules\Exchange\Services\Api\Exmo\GetExchangeInfo::run();
        \Modules\Exchange\Services\Api\Kraken\GetExchangeInfo::run();
    }
}
