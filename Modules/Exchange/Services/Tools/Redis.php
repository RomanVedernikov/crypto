<?php

namespace Modules\Exchange\Services\Tools;

use Modules\Logger\Services\Logger;

class Redis
{
    const ERROR_MESSAGE_PREDIS_READONLY = 'READONLY You can\'t write against a read only replica.';

    public function __construct()
    {
    }

    public static function set($sHash, $sKey, $mxData)
    {
        if (is_array($mxData)) {
            $mxData = json_encode($mxData);
        }
        try {

            \Redis::hSet($sHash, $sKey, $mxData);

        } catch (\Predis\Response\ServerException $e) {

            // Ловим ошибку: READONLY You can't write against a read only replica
            // Отвалился Redis, его взломали, проверь и поменяй пароль!
            if ($e->getMessage() == static::ERROR_MESSAGE_PREDIS_READONLY) {
                //dump($e);
                Logger::write("Redis: " . $e->getMessage());

                // Отрубим все команды
                $obKillAllComands = new \Modules\Command\Services\KillAll();
                $obKillAllComands->run();

                // ToDo: сообщение админу

            }
            dd("Error: " . $e->getMessage());
        }
    }

    public static function setMany($arDataCash)
    {
        if (empty($arDataCash)) {
            return false;
        }
        foreach ($arDataCash as $arDataCashItem) {
            self::set($arDataCashItem['sHash'], $arDataCashItem['sKey'], $arDataCashItem['arData']);
        }
    }
}
