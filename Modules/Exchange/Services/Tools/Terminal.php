<?php

namespace Modules\Exchange\Services\Tools;


class Terminal
{

    public static function startBackgroundProcess($command)
    {
        if (empty($command)) {
            return false;
        }
        if (self::isWindows()) {
            //$command = 'start "' . $command . '" ' . $command;
            pclose(popen("start /B " . $command, "r"));
        } else {
            exec($command . " > /dev/null &");
        }
        return true;
    }

    public static function checkProcessByPid($pid)
    {
        if (empty($pid)) {
            return false;
        }
        if (self::isWindows()) {
            $command = "tasklist /FI \"PID eq {$pid}\"";
        } else {
            $command = "ps -p {$pid}";
        }
        exec($command, $output);
        if (count($output) > 1) {
            return true;
        }
        return false;
    }

    public static function killProcessByPid($pid)
    {
        if (empty($pid)) {
            return false;
        }
        // Может уже завершена
        if (!self::checkProcessByPid($pid)) {
            return true;
        }
        // Убиваем
        if (self::isWindows()) {
            $command = "Taskkill /PID {$pid} /F";
        } else {
            $command = "kill {$pid}";
        }
        exec($command, $output);
        // Проверяем
        if (!self::checkProcessByPid($pid)) {
            return true;
        }
        return false;
    }

    public static function checkProcessByName($comand_name)
    {
        if (empty($comand_name)) {
            return false;
        }
        if (self::isWindows()) {
            $command = "";
        } else {
            $command = "pgrep '{$comand_name}'";
        }
        exec($command, $output);
        if (count($output) > 1) {
            return true;
        }
        return false;
    }

    public static function killProcessByName($comand_name)
    {
        if (empty($comand_name)) {
            return false;
        }
        if (self::isWindows()) {
            $command = "";
        } else {
            $command = "killall '{$comand_name}' > /dev/null &";
        }
        exec($command);
        if (self::checkProcessByName($comand_name) == false) {
            return true;
        }
        return false;
    }

    public static function isWindows()
    {
        return PHP_OS == 'WINNT' || PHP_OS == 'WIN32' || PHP_OS == 'Windows';
    }
}
