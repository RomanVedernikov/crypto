<?php

namespace Modules\Exchange\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExchangeInfo extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'exchange_info';

    protected $fillable = [
        'exchange_id',
        'symbol',
        'status',
        'ws_symbol', // kraken
        'base_asset',
        'quote_asset',
        'base_asset_precision',
        'quote_asset_precision',
        'order_types',
        'permissions',
        'is_spot_trading'
    ];

    protected static function newFactory()
    {
        return \Modules\Exchange\Database\factories\SymbolExchangeInfoFactory::new();
    }
}
