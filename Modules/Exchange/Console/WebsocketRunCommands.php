<?php

namespace Modules\Exchange\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Modules\Exchange\Services\WebSockets\Command\WebsocketRunCommands as WSRunCommands;

class WebsocketRunCommands extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ws:run-commands';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Запускает все команды вебсокетов для отслеживания параметров. Проверяет уже запущенные по сигнатуре и pid';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $obWSRunCommands = new WSRunCommands();
        $obWSRunCommands->run();
    }
}
