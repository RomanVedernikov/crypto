<?php

namespace Modules\Exchange\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Modules\Exchange\Services\WebSockets\Command\WebsocketSpotBookCommand;

class WebsocketSpotBook extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ws:spot-book {exchange_code} {script_index?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Цена и объем для продажи/покупки в текущий момент, с нужной глубиной.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arOptions = [
            'arguments' => $this->arguments(),
            'options' => $this->options(),
            'output' => $this->output,
            'input' => $this->input
        ];
        $obWebsocketSpotBookCommand = new WebsocketSpotBookCommand($arOptions);
        $obWebsocketSpotBookCommand->run();

        return 0;
    }
}
