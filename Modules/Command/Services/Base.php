<?php
namespace Modules\Command\Services;

use Modules\Command\Entities\Command as CommandEntity;
use Modules\Exchange\Services\Command\CheckAndRun;
use Modules\Exchange\Services\Tools\Terminal;

class Base
{
    public $arObCommands;

    public function getCommands()
    {
        if (!is_null($this->arObCommands)) {
            return $this->arObCommands;
        }
        $arObCommands = CommandEntity::all();
        $this->arObCommands = $arObCommands ?? [];
        return $this->arObCommands;
    }

    public function killProcessCheckAndRun()
    {
        // ToDo: доделать эту ф-ю, пока она не работает
        //Terminal::killProcessByName('php artisan command:check-and-run');
        // Пока сделаем файлом
        return Terminal::killProcessByPid(CheckAndRun::getCommandPid());
    }
}
