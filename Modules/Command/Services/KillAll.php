<?php

namespace Modules\Command\Services;

use Laravel\Telescope\Telescope;
use Modules\Command\Entities\Command as CommandEntity;

class KillAll extends Base
{

    public function __construct()
    {
    }

    public function run()
    {
        Telescope::stopRecording();

        // Первым делом нужно убить команду, которая перезапускает другие команды
        $this->killProcessCheckAndRun();
        sleep(2);

        try {
            /** @var CommandEntity $obCommand */
            foreach ($this->getCommands() as $obCommand) {

                $obCommand->stopCommand();
            }
        } catch (\ErrorException $e) {

            dd("Error: " . $e->getMessage());
        }
    }
}
