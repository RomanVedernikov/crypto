<?php

namespace Modules\Command\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Exchange\Services\Tools\Terminal;

class Command extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'pid',
        'command',
        'signature',
        'last_action',
        'last_action_at',
        'exit_at',
        'error_exception',
        'is_kill',
    ];

    public static function deleteAll()
    {
        self::truncate();
    }

    public function checkIsRun()
    {
        if ($this->pid) {
            return Terminal::checkProcessByPid($this->pid);
        }
        return false;
    }

    public function runCommand()
    {
        $command = "php artisan " . $this->command;
        $bIsStart = Terminal::startBackgroundProcess($command);
        if ($bIsStart) {
            $this->delete();
            return true;
        }
        return false;
    }

    /**
     * Перезапускат только если команда уже не работает
     */
    public function restartCommand()
    {
        if ($this->checkIsRun()) {
            return false;
        }
        return $this->runCommand();
    }

    public function stopCommand()
    {
        $bIsKill = Terminal::killProcessByPid($this->pid);
        if ($bIsKill) {
            $this->is_kill = true;
            $this->save();
            $this->delete();
            return true;
        }
        return false;
    }

    protected static function newFactory()
    {
        return \Modules\Command\Database\factories\CommandFactory::new();
    }
}
