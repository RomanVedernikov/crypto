<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommandTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commands', function (Blueprint $table) {
            $table->id();
            $table->integer('pid');
            $table->string('command');
            $table->string('signature')->nullable();
            $table->string('last_action')->nullable();
            $table->timestamp('last_action_at')->nullable();
            $table->timestamp('exit_at')->nullable();
            $table->text('error_exception')->nullable();
            $table->boolean('is_kill')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('command');
    }
}
