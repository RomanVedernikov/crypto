<?php

namespace Modules\Command\Console;

use Illuminate\Console\Command;
use Modules\Command\Services\RestartAll;

class RestartAllCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:restart-all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Следит за командами которые отвалились и перезапускает их';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arOptions = [
            'output' => $this->output,
            'input' => $this->input
        ];
        $obRestartAll = new RestartAll($arOptions);
        $obRestartAll->run();
    }
}
