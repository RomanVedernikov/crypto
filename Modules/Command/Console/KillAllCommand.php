<?php

namespace Modules\Command\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Modules\Exchange\Services\Command\CheckAndRun;
use Modules\Command\Services\KillAll;

class KillAllCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:kill-all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Следит за командами которые отвалились и перезапускает их';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arOptions = [
            'output' => $this->output,
            'input' => $this->input
        ];
        $obKillAll = new KillAll($arOptions);
        $obKillAll->run();
    }
}
