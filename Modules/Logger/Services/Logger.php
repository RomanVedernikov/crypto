<?php

namespace Modules\Logger\Services;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

class Logger
{
    public $chanel = null;

    public function __construct()
    {

    }

    private function getChannel()
    {
        if (!is_null($this->chanel)) {
            return $this->chanel;
        }
        $this->chanel = Log::build([
            'driver' => 'single',
            'path' => self::getLogPath(),
        ]);
        return $this->chanel;
    }

    public function info(string $message, $context = [])
    {
        Log::stack([$this->getChannel()])->info($message, $context);
    }

    public static function write(string $message, $context = [])
    {
        $chanel = Log::build([
            'driver' => 'single',
            'path' => self::getLogPath(),
        ]);
        Log::stack([$chanel])->info($message, $context);
    }

    public static function getLogPath()
    {
        $date = Carbon::now()->format('d.m.Y');
        return storage_path("logs/my_log/{$date}/logger.log");
    }

}
